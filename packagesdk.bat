@echo off
echo x64dbg plugin sdk for JWasm64 - Creating SDK
echo.
echo Creating SDK...
del .\downloads\x64dbg-plugin-sdk-for-jwasm64.zip > nul
"Z:\Program Files\Compression Programs\WinRAR\WinRar.exe" a -m5 -r .\downloads\x64dbg-plugin-sdk-for-jwasm64.zip x64dbg-pluginsdk-readme.txt .\extras\*.* .\pluginsdk\*.* .\testplugin\*.* 
echo.
echo.Finished