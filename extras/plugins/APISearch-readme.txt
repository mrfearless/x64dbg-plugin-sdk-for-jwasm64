;=====================================================================================
;
; APISearch-readme.txt
;
;-------------------------------------------------------------------------------------

About
-----

APISearch plugin for x64dbg (64bit plugin), created with the x64dbg plugin for JWasm64
by fearless 2016 - www.LetTheLight.in

The x64dbg plugin for JWasm64 can be downloaded from here:
https://bitbucket.org/mrfearless/x64dbg-plugin-sdk-for-JWasm64/overview


Overview
--------

A plugin to allow searching for API calls and/or searching online from command bar


Features
--------

- Search online for API calls in the dissassembly window (lines that begin with 'call')
- Search from the command bar using google, msdn or pinvoke, ie: 'google <searchterm>'
- Open web browser to google, msdn or pinvoke, ie: 'msdn' opens browser at msdn.microsoft.com


Notes
-----